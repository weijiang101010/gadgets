#!/bin/bash

# This script runs astyle and complains about lines that are too long
# over all files ending in .h, .c, .hpp.cpp listed by git in the given
# directory.

# Check for the latest astyle version
ASTYLE_VER_REQUIRED_3="Artistic Style Version 3.0"
ASTYLE_VER_REQUIRED_3_1_beta="Artistic Style Version 3.1 beta"
ASTYLE_VER_REQUIRED_3_1="Artistic Style Version 3.1"
ASTYLE_VER=`astyle --version`
if [ "$ASTYLE_VER" = "$ASTYLE_VER_REQUIRED_3" ] ; then
    echo "$ASTYLE_VER"
elif [ "$ASTYLE_VER" = "$ASTYLE_VER_REQUIRED_3_1_beta" ] ; then
    echo "$ASTYLE_VER"
elif [ "$ASTYLE_VER" = "$ASTYLE_VER_REQUIRED_3_1" ] ; then
    echo "$ASTYLE_VER"
else
    echo "Error: you're using ${ASTYLE_VER}"
    echo "This is old,please update it"
    exit 1
fi

if [ $# = 0 ] ; then
    echo "Pls input files or dirs"
    exit 1
fi

MYSTYLERC=`pwd`/.mystylerc
echo "--lineend=linux" > $MYSTYLERC
echo "--indent=spaces=4" >> $MYSTYLERC
echo "--style=linux" >> $MYSTYLERC
echo "--pad-oper" >> $MYSTYLERC
echo "--pad-header" >> $MYSTYLERC
echo "--unpad-paren" >> $MYSTYLERC
echo "--align-pointer=name" >> $MYSTYLERC
echo "--align-reference=name" >> $MYSTYLERC
echo "--add-brackets" >> $MYSTYLERC

STYLERC=$MYSTYLERC

handl_file()
{
  dos2unix $1 > /dev/null 2>&1
  astyle  --options=$STYLERC $1 > /dev/null 2>&1
  chmod 644 $1 2>&1
  rm -f ${1}.orig
}

handl_line()
{
    count=1
    while IFS= read -r line; 
    do
        # Check for lines too long
        len=${#line}
        if [ $len -gt 100 ]; then
            echo "$1 Line $count too long"
            error_found=true
        fi
        (( count++ ))
    done < "$1"
}

hand_file_form_list()
{
    while read line
    do
       handl_file $line
       handl_line $line
    done < $1
}

TEMPFILE=.styletmpfile
rm -f $TEMPFILE

for i in $@
do
    if [ -f $i ] ; then
        TAIL=`echo ${i##*.}`
        TAIL_FLAG=0;
        if [ $TAIL == $i ] ; then
            echo "unknow format of file $i"
        elif [ $TAIL == "c" ] ; then
            TAIL_FLAG=1;
        elif [ $TAIL == "h" ] ; then
            TAIL_FLAG=1;
        elif [ $TAIL == "cpp" ] ; then
            TAIL_FLAG=1;
        elif [ $TAIL == "hpp" ] ; then
            TAIL_FLAG=1;
        fi
        if [ $TAIL_FLAG == 1 ] ; then
            handl_file $i
            handl_line $i
        else
            echo "$i is not support"
        fi
    elif [ -d $i ] ; then     
        find $i -name "*.c"   -exec echo '{}' >> $TEMPFILE \;
        find $i -name "*.h"   -exec echo '{}' >> $TEMPFILE \;
        find $i -name "*.cpp" -exec echo '{}' >> $TEMPFILE \;
        find $i -name "*.hpp" -exec echo '{}' >> $TEMPFILE \;
        hand_file_form_list $TEMPFILE
        rm -f $TEMPFILE
    else
        echo "The file $i is no exist"
    fi 
done

rm -f $MYSTYLERC
